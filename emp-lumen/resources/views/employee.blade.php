
@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Employee</div>

                <div class="panel-body">

                    <h3>Test </h3>
                    <label>Collection:</label>

                    <br><br>
                    <h3>Test wih cURL</h3>

                    <pre class="line-numbers language-php" data-start="1">
                    <code class="language-php">
                            $curl = curl_init();

                            curl_setopt_array($curl, [
                              CURLOPT_RETURNTRANSFER => true,
                              CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                              CURLOPT_CUSTOMREQUEST => "GET",
                              CURLOPT_HTTPHEADER => ["content-type: application/json"],
                            ]);

                            $response = curl_exec($curl);
                            $err = curl_error($curl);

                            curl_close($curl);

                            if ($err)
                              echo "cURL Error #:" . $err;
                            else
                              echo $response;
                        </code>
                    </pre>
            </div>
        </div>
    </div>
</div>
@endsection
