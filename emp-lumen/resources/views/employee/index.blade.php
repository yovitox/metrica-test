@extends('app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <div class="panel panel-default">
                <div class="panel-heading">Employees</div>


                <div class="panel-body">
                    <form action="{!! route('employees.index') !!}">
                        <div class="form-inline pull-right">
                            <div class="form-group">
                                <input type="text" name="email" class="form-control"
                                    value="{{ request()->input('email') }}" placeholder="search for email"/>
                                <button type="submit" class="btn btn-success">Search</button>
                                <a href="{!! route('employees.index') !!}" class="btn btn-default">Clean</a>
                            </div>
                        </div>

                        <br><br><br>
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Position</th>
                                    <th>Salary</th>
                                    <th>Actions</th>
                                  </tr>
                            </thead>
                            <tbody>
                                @foreach ($employees as $employee)
                                <tr>
                                    <td>{{ $employee->name }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->position }}</td>
                                    <td>$ {{ $employee->salary }}</td>
                                    <td><a href="{!! route('employees.show', ['id' => $employee->_id]) !!}"
                                        type="button" class="btn btn-primary">View</button></td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
