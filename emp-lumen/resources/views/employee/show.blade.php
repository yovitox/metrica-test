@extends('app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading"><a href="{!! route('employee.index') !!}" class="btn btn-default"> < </a> {{ $employee->name }}</div>

                <div class="panel-body">


                    <form class="form-horizontal">
                        <div class="form-group">
                          <label for="name" class="col-sm-2 control-label">Name: </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="name" placeholder="Name" value="{{ $employee->name }}" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="email" class="col-sm-2 control-label">Email: </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="email" placeholder="Email" value="{{ $employee->email }}" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="phone" class="col-sm-2 control-label">Phone: </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="phone" placeholder="Phone" value="{{ $employee->phone }}" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="address" class="col-sm-2 control-label">Address: </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="address" placeholder="Address" value="{{ $employee->address }}" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="position" class="col-sm-2 control-label">Position: </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="position" placeholder="Position" value="{{ $employee->position }}" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                          <label for="salary" class="col-sm-2 control-label">Salary: </label>
                          <div class="col-sm-10">
                            <input type="email" class="form-control" id="salary" placeholder="Salary" value="$ {{ $employee->salary }}" disabled>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="phone" class="col-sm-2 control-label">Skills: </label>
                            <div class="col-sm-10">
                                <div class="row">
                                    @foreach ($employee->skills as $skill)
                                    <div class="col-sm-2">
                                        <input type="email" class="form-control" id="salary" placeholder="Salary" value="{{ $skill['skill'] }}" disabled><br>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
