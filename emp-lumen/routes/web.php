<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => config('app.api_prefix') . config('app.api_version')], function ($router) {
  $router->get('employee', 'EmployeeController@index');
  $router->get('employee/{email}', 'EmployeeController@show');
});
