<?php
namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;
use App\Traits\JsonResponser;
class EmployeeController extends Controller{

    use JsonResponser;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) {
        $validatedData = $request->validate([
            'email' => 'email|exists:employee',
        ]);

        $employees = ($request->email) ? Employee::where('isOnline', true)
            ->where('email', $request->email)->get() : Employee::where('isOnline', true)->get();

    }

    /**
     * Display a filter of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function filter(Request $request){
        $employees = Employee::where('isOnline', true);
        if (isset($request->min)) $employees->where('salary', '>', (int) $request->min);
        if (isset($request->max)) $employees->where('salary', '<', (int) $request->max);

        return $employees->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    /**
     * Display the specified resource.
     *
     * @param  \Metrica\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {

        return view('employee.show', compact('employee'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \Metrica\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {
        //
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Metrica\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Employee $employee)
    {
        //
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \Metrica\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        //
    }
}
