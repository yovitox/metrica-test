<?php
/*******************************
 *  class.clearpar.php    *
 *******************************/

class ClearPar {

  var $open = "(";
  var $close = ")";

  public function build ( $value ){

      $final = "";
      $len = strlen( $value ) ;
      for ( $i=0; $i <= $len-1; $i++ ) {
        $final .= $this->isComplete( $value[$i], $value[$i + 1] );
      }

      echo $final;

  }

  private function isComplete( $value, $next ) {

  		if ( $value === $this->open ) {
  			if ( $next === $this->close ) {
  				return $value . $next;
  			} else {
  				return null;
  			}
  		} else {
  			return null;
  		}

  }

}
