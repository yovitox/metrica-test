<?php
/*******************************
 *  class.completerange.php    *
 *******************************/
class completeRange{

  public function build( $value ){

      $min = min( $value );
      $max = max( $value );
      $newList = Array();
      $this->isValid( $min );
      for ( $i = $min;  $i<=$max; $i++  ){
        array_push( $newList, $i );
      }

      echo implode( ",", $newList )
      . "</br> <b>Nota:</b> cargue la lista en javascript,
      trate de enviarla como parametro al php y por falta de tiempo no lo conseguí.
      Lo que se muestra es una lista con el nombre 'range' en el archivo index.php linea 23.";

  }

  private function isValid ( $value ){

      if ( $value < 0 ) {
        return null;
      }elseif ( !is_int( $value ) ) {
        return null;
      }

  }

}
