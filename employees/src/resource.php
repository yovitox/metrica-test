<?php
namespace Application\Services;
/**
 * Clase Resource.
 */
class Resource{

  const SOURCE_PATH = __DIR__.'/../db/employees.json';
  /**
   * get all the employees
   */
  public static function showAll()
  {
    $content = json_decode(file_get_contents(self::SOURCE_PATH));
    return $content;
  }
  /**
   * get emp by email.
   */
  public static function findByEmail($email){

    $source = self::showAll();
    if (strlen($email) == 0) return $source;

    $result = [];
    foreach ($source as $key => $value) {
      if (strpos($value->email, $email) !== false)$result[] = $value;
    }
    return $result;

  }

  public function findBySalary($min, $max){
    $source = self::showAll();
    $result = [];
    foreach ($source as $key => $value) {
      $salary = preg_replace("/([^0-9\\.])/", "", $value->salary);
      if ($salary > $min && $salary < $max) {
        $result[] = $value;
      }
    }
    return $result;
  }

}
