<?php

use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get(['GET', 'POST'], function (Request $request, Response $response, array $args) {

  if ($request->isPost()) {
    $params = $request->getParsedBody();
    $jsonContent = \Application\Services\Resource::findByEmail($params['search-term']);
  } else {
    $jsonContent = \Application\Services\Resource::showAll();
  }
  return $this->view->render($response, 'index.twig', [
    'page_title' => 'Employees List',
    'employees' => $jsonContent
  ]);

  $app->get('/employee/{id}', function ($request, $response, $args){

  $employee = \Application\Services\Resource::findById($args['id']);
  return $this->view->render($response, 'detail.twig', [
    'page_title' => 'Employee Detail',
    'data' => $employee,
    ]);
  });

  $app->get('/employee/{min}/{max}', function ($request, $response, $args)
  {
    $jsonContent = \Application\Services\Resource::findBySalary( $args['min'], $args['max']);
    $res = $response->withHeader('Content-type', 'text/xml');
    return $this->view->render($res, 'responseXML.twig', [
      'page_title' => 'Employee List',
      'employees' => $jsonContent
    ]);
  });


});
